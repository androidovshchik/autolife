# eventbus
-keep enum org.greenrobot.eventbus.ThreadMode { *; }
-keepattributes *Annotation*
-keepclassmembers class ** { @org.greenrobot.eventbus.Subscribe <methods>; }