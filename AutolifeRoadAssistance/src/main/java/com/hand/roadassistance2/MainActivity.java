package com.hand.roadassistance2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.android.gms.location.LocationRequest;
import com.yayandroid.locationmanager.LocationManager;
import com.yayandroid.locationmanager.configuration.DefaultProviderConfiguration;
import com.yayandroid.locationmanager.configuration.GooglePlayServicesConfiguration;
import com.yayandroid.locationmanager.configuration.LocationConfiguration;
import com.yayandroid.locationmanager.configuration.PermissionConfiguration;
import com.yayandroid.locationmanager.constants.FailType;
import com.yayandroid.locationmanager.constants.ProcessType;
import com.yayandroid.locationmanager.constants.ProviderType;
import com.yayandroid.locationmanager.listener.LocationListener;

import java.util.Locale;

public class MainActivity extends BaseActivity implements ActivityCompat.OnRequestPermissionsResultCallback,
    LocationListener {

    public static final int CODE_PERMISSION = 1;

    public static final int LOCATION_UPDATE_FREQUENCY = 5000;

    private static final String SMS_TO_DEFAULT_PHONE_NUMBER = "sms:+35795112244";
    //private static final String template_reserved = "NEED HELP ON ROAD!\nGoogle location: https://www.google.com/maps?daddr=%f,%f\n2GIS location: https://2gis.ru/geo/%f,%f?queryState=center/%f,%f/zoom/16\nName: %s\nPhone: %s\nReg. number: %s\nBrand: %s\nModel: %s";
    private static final String template = "NEED HELP ON ROAD!\nGoogle location: https://www.google.com/maps?daddr=%f,%f\n2GIS location: dgis://2gis.ru/routeSearch/rsType/car/to/%f,%f\nName: %s\nPhone: %s\nReg. number: %s\nBrand: %s\nModel: %s";

    public double latitude;
    public double longitude;

    private Boolean gpsEnabled;

    private ProgressBar progressBar;
    private Button circle;

    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBar = findViewById(R.id.progress_bar);
        circle = findViewById(R.id.circle);
        initBase();
        loadPreferences();
        LocationManager.enableLog(BuildConfig.DEBUG);
        locationManager = new LocationManager.Builder(getApplicationContext())
            .configuration(getConfiguration())
            .activity(this)
            .notify(this)
            .build();
    }

    @Override
    public void onStart() {
        super.onStart();
        locationManager.get();
        gpsEnabled = checkGPSEnabled();
        if (gpsEnabled) {
            onWaitGPS();
        } else {
            onGPSTurnedOff();
        }
    }

    @CallSuper
    @Override
    public void onPause() {
        super.onPause();
        locationManager.onPause();
    }

    @CallSuper
    @Override
    public void onResume() {
        super.onResume();
        locationManager.onResume();
        if (getIntent().hasExtra("url")) {
            Intent webIntent = new Intent(getApplicationContext(), WebActivity.class);
            webIntent.putExtra("url", getIntent().getStringExtra("url"));
            webIntent.putExtra("clear", getIntent().getBooleanExtra("clear", false));
            webIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getIntent().removeExtra("url");
            startActivity(webIntent);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            gpsEnabled = checkGPSEnabled();
            if (gpsEnabled) {
                onWaitGPS();
            } else {
                onGPSTurnedOff();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CODE_PERMISSION:
                if ((data == null) || (resultCode == RESULT_CANCELED)) {
                    return;
                }
                String varName = data.getStringExtra(KEY_NAME);
                String varPhone = data.getStringExtra(KEY_PHONE);
                String varVehicleID = data.getStringExtra(KEY_VEHICLE_ID);
                String varVehicleMark = data.getStringExtra(KEY_VEHICLE_MARK);
                String varVehicleModel = data.getStringExtra(KEY_VEHICLE_MODEL);
                String sms = String.format(Locale.US, template, latitude, longitude, longitude,
                    latitude, varName, varPhone, varVehicleID, varVehicleMark, varVehicleModel);
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse(SMS_TO_DEFAULT_PHONE_NUMBER));
                intent.putExtra("sms_body", sms);
                startActivity(intent);
                break;
            default:
                locationManager.get();
                locationManager.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_done:
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean(KEY_FIRST_START, false).apply();
                findViewById(R.id.startUp).setVisibility(View.GONE);
                findViewById(R.id.btn_done).setVisibility(View.GONE);
                break;
            case R.id.circle:
                Intent intent = new Intent(getApplicationContext(), PermissionActivity.class);
                startActivityForResult(intent, CODE_PERMISSION);
                break;
            default:
                super.onClick(v);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_NAME, fieldName);
        editor.putString(KEY_PHONE, fieldPhone);
        editor.putString(KEY_VEHICLE_ID, fieldVehicleID);
        editor.putString(KEY_VEHICLE_MARK, fieldVehicleMark);
        editor.putString(KEY_VEHICLE_MODEL, fieldVehicleModel);
        editor.apply();
    }

    @Override
    @SuppressWarnings("all")
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        locationManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void loadPreferences() {
        boolean firstStart = sharedPreferences.getBoolean(KEY_FIRST_START, true);
        if (firstStart) {
            findViewById(R.id.startUp).setVisibility(View.VISIBLE);
            findViewById(R.id.btn_done).setVisibility(View.VISIBLE);
        }
        fieldName = sharedPreferences.getString(KEY_NAME, "");
        fieldPhone = sharedPreferences.getString(KEY_PHONE, "");
        fieldVehicleID = sharedPreferences.getString(KEY_VEHICLE_ID, "");
        fieldVehicleMark = sharedPreferences.getString(KEY_VEHICLE_MARK, "");
        fieldVehicleModel = sharedPreferences.getString(KEY_VEHICLE_MODEL, "");
    }

    private boolean checkGPSEnabled() {
        String provider = Settings.Secure.getString(getContentResolver(),
            Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        return provider.contains("gps");
    }

    private void onWaitGPS() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void onGPSTurnedOff() {
        circle.setEnabled(false);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            circle.setEnabled(true);
            progressBar.setVisibility(View.GONE);
        } else {
            circle.setEnabled(false);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onLocationFailed(@FailType int failType) {
        gpsEnabled = checkGPSEnabled();
        if (gpsEnabled) {
            onWaitGPS();
        } else {
            onGPSTurnedOff();
        }
    }

    @Override
    public void onProcessTypeChanged(@ProcessType int processType) {}

    @Override
    public void onPermissionGranted(boolean alreadyHadPermission) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onProviderDisabled(String provider) {}

    @SuppressWarnings("unused")
    public LocationConfiguration getConfiguration() {
        return new LocationConfiguration.Builder()
            .keepTracking(true)
            .askForPermission(new PermissionConfiguration.Builder().build())
            .useGooglePlayServices(new GooglePlayServicesConfiguration.Builder()
                .locationRequest(LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(LOCATION_UPDATE_FREQUENCY))
                .fallbackToDefault(true)
                .setWaitPeriod(0)
                .build())
            .useDefaultProviders(new DefaultProviderConfiguration.Builder()
                .requiredTimeInterval(LOCATION_UPDATE_FREQUENCY)
                .setWaitPeriod(ProviderType.GPS, 0)
                .build())
            .build();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        locationManager.onDestroy();
    }
}