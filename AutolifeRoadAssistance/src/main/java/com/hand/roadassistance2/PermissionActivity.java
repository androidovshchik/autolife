package com.hand.roadassistance2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class PermissionActivity extends BaseActivity {

    EditText editName;
    EditText editPhone;
    EditText editVehicleID;
    EditText editVehicleMark;
    EditText editVehicleModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission);
        editName = findViewById(R.id.name);
        editPhone = findViewById(R.id.phone);
        editVehicleID = findViewById(R.id.vehicle_id);
        editVehicleMark = findViewById(R.id.vehicle_mark);
        editVehicleModel = findViewById(R.id.vehicle_model);
        getSettings();
        initBase();
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        switch (v.getId()) {
            case R.id.btn_ok:
                intent.putExtra(MainActivity.KEY_NAME, editName.getText().toString());
                intent.putExtra(MainActivity.KEY_PHONE, editPhone.getText().toString());
                intent.putExtra(MainActivity.KEY_VEHICLE_ID, editVehicleID.getText().toString());
                intent.putExtra(MainActivity.KEY_VEHICLE_MARK, editVehicleMark.getText().toString());
                intent.putExtra(MainActivity.KEY_VEHICLE_MODEL, editVehicleModel.getText().toString());
                setResult(RESULT_OK, intent);
                finish();
                return;
            case R.id.btn_ok2:
                intent.putExtra(MainActivity.KEY_NAME, editName.getText().toString());
                intent.putExtra(MainActivity.KEY_PHONE, editPhone.getText().toString());
                intent.putExtra(MainActivity.KEY_VEHICLE_ID, editVehicleID.getText().toString());
                intent.putExtra(MainActivity.KEY_VEHICLE_MARK, editVehicleMark.getText().toString());
                intent.putExtra(MainActivity.KEY_VEHICLE_MODEL, editVehicleModel.getText().toString());
                setResult(RESULT_OK, intent);
                finish();
                return;
            case R.id.btn_cancel:
                setResult(RESULT_CANCELED, intent);
                finish();
                return;
            default:
                super.onClick(v);
        }
    }

    private void getSettings() {
        editName.setText(fieldName);
        editPhone.setText(fieldPhone);
        editVehicleID.setText(fieldVehicleID);
        editVehicleMark.setText(fieldVehicleMark);
        editVehicleModel.setText(fieldVehicleModel);
    }
}
