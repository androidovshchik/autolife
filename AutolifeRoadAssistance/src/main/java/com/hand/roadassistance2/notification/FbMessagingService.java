package com.hand.roadassistance2.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.hand.roadassistance2.MainActivity;
import com.hand.roadassistance2.R;
import com.hand.roadassistance2.WebActivity;

import org.greenrobot.eventbus.EventBus;

public class FbMessagingService extends FirebaseMessagingService {

    @Override
    @SuppressWarnings("all")
    public void onMessageReceived(RemoteMessage message) {
        RemoteMessage.Notification notification = message.getNotification();
        if (notification != null) {
            SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
            int count = sharedPreferences.getInt(WebActivity.KEY_BELLS_COUNT, 0);
            sharedPreferences.edit().putInt(WebActivity.KEY_BELLS_COUNT, count + 1).apply();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationManager notificationManager = (NotificationManager)
                    getSystemService(Context.NOTIFICATION_SERVICE);
                NotificationChannel notificationChannel = new NotificationChannel(getString(R.string.app_name),
                    getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(notificationChannel);
            }
            showNotification(notification);
            if (EventBus.getDefault().hasSubscriberForEvent(Boolean.class)) {
                EventBus.getDefault().post(new Boolean(true));
            }
        }
    }

    @SuppressWarnings("all")
    private void showNotification(RemoteMessage.Notification message) {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("url", "http://newautolife.com/offers-app.html?type=app");
        intent.putExtra("clear", true);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0,
            intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(),
            getString(R.string.app_name))
            .setSmallIcon(R.drawable.wheel)
            .setContentTitle(message.getTitle())
            .setContentText(message.getBody())
            .setAutoCancel(true)
            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notificationBuilder.build());
    }
}