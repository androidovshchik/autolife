package com.hand.roadassistance2;

import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.hand.roadassistance2.other.FontManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String DIAL_DEFAULT_PHONE_NUMBER = "tel:+35795112244";

    public static final int CODE_SETTINGS = 0;

    public static final String KEY_BELLS_COUNT = "Bells_count";
    public static final String KEY_FIRST_START = "First_start";
    public static final String KEY_NAME = "Name";
    public static final String KEY_PHONE = "Phone";
    public static final String KEY_VEHICLE_ID = "Vehicle number";
    public static final String KEY_VEHICLE_MARK = "Vehicle mark";
    public static final String KEY_VEHICLE_MODEL = "Vehicle model";

    public static String fieldName;
    public static String fieldPhone;
    public static String fieldVehicleID;
    public static String fieldVehicleMark;
    public static String fieldVehicleModel;

    protected SharedPreferences sharedPreferences;

    protected DrawerLayout drawer;

    protected NotificationManager notificationManager;

    @SuppressWarnings("all")
    protected void initBase() {
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        FontManager.markAsIconContainer(findViewById(R.id.bottomBar),
            FontManager.getTypeface(getApplicationContext(), FontManager.FONT_AWESOME));
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer,
            (Toolbar) findViewById(R.id.toolbar), 0, 0);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        ((NavigationView) findViewById(R.id.nav_view))
            .setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    String url = null;
                    Intent intent;
                    switch (item.getItemId()) {
                        case R.id.nav_about:
                            url = "http://newautolife.com/about-app.html?type=app";
                            break;
                        case R.id.nav_sign_up:
                            url = "http://newautolife.com/makeservice-app.html?type=app";
                            break;
                        case R.id.nav_office:
                            url = "http://newautolife.com/cab-app2.html?type=app";
                            break;
                        case R.id.nav_actions:
                            sharedPreferences.edit().remove(KEY_BELLS_COUNT).apply();
                            findViewById(R.id.count).setVisibility(View.GONE);
                            notificationManager.cancelAll();
                            url = "http://newautolife.com/offers-app.html?type=app";
                            break;
                        case R.id.nav_contacts:
                            url = "http://newautolife.com/contact-app.html?type=app";
                            break;
                        case R.id.nav_settings:
                            intent = new Intent(getApplicationContext(), SettingsActivity.class);
                            startActivityForResult(intent, MainActivity.CODE_SETTINGS);
                            break;
                    }
                    if (url != null) {
                        intent = new Intent(getApplicationContext(), WebActivity.class);
                        intent.putExtra("url", url);
                        startActivity(intent);
                    }
                    drawer.closeDrawer(GravityCompat.START);
                    if (!BaseActivity.this.getClass().equals(MainActivity.class)) {
                        finish();
                    }
                    return true;
                }
            });
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        notifyBellsCount();
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBellEvent(Boolean event) {
        notifyBellsCount();
    }

    protected void notifyBellsCount() {
        int count = sharedPreferences.getInt(KEY_BELLS_COUNT, 0);
        if (count > 0) {
            TextView countView = findViewById(R.id.count);
            countView.setVisibility(View.VISIBLE);
            countView.setText(count > 9 ? "9+" : "" + count);
        } else {
            findViewById(R.id.count).setVisibility(View.GONE);
        }
    }

    @Override
    @SuppressWarnings("all")
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.btn_globe:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://newautolife.com/?type=app")));
                break;
            case R.id.btn_bell:
                sharedPreferences.edit().remove(KEY_BELLS_COUNT).apply();
                findViewById(R.id.count).setVisibility(View.GONE);
                notificationManager.cancelAll();
                intent = new Intent(getApplicationContext(), WebActivity.class);
                intent.putExtra("url", "http://newautolife.com/offers-app.html?type=app");
                startActivity(intent);
                if (!getClass().equals(MainActivity.class)) {
                    finish();
                }
                break;
            case R.id.btn_home: case R.id.top_logo:
                if (!getClass().equals(MainActivity.class)) {
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    finish();
                }
                break;
            case R.id.btn_facebook:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/newautolife")));
                break;
            case R.id.btn_phone:
                try {
                    intent = new Intent(Intent.ACTION_DIAL, Uri.parse(DIAL_DEFAULT_PHONE_NUMBER));
                    startActivity(intent);
                }catch (ActivityNotFoundException e) {
                    Toast.makeText(getApplicationContext(), "No Activity found to handle Intent",
                        Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((data == null) || (resultCode == RESULT_CANCELED)) {
            return;
        }
        switch (requestCode) {
            case CODE_SETTINGS:
                fieldName = data.getStringExtra(KEY_NAME);
                fieldPhone = data.getStringExtra(KEY_PHONE);
                fieldVehicleID = data.getStringExtra(KEY_VEHICLE_ID);
                fieldVehicleMark = data.getStringExtra(KEY_VEHICLE_MARK);
                fieldVehicleModel = data.getStringExtra(KEY_VEHICLE_MODEL);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
